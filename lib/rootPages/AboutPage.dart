import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../helpers/header.dart';
import '../main.dart';

class AboutPage extends StatefulWidget {
  // Here will be code
  @override
  _NewPageState createState() => _NewPageState();
}

class _NewPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //topBar
      appBar: headerNav(),
      //sidebar
      drawer: sideBar(context),

      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 50),
            Text("About complusiT"),
            SizedBox(height: 50),
            RaisedButton(
              child: Text("About complusiT"),
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    PageTransition(
                        type: PageTransitionType.fade,
                        duration: Duration(milliseconds: 10),
                        child: MainPage()),
                    (r) => true);
              },
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '../helpers/header.dart';

class InfoPage extends StatefulWidget {
  // Here will be code
  @override
  _NewPageState createState() => _NewPageState();
}

class _NewPageState extends State<InfoPage> {
  // Here will be code

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //topBar
        appBar: headerNav(),
        //sidebar
        drawer: sideBar(context),
        body: Center(
          child: Column(
            children: <Widget>[
              SizedBox(height: 50),
              Text("Info complusiT"),
              SizedBox(height: 50),
              RaisedButton(
                child: Text("This is a Info for your terms of Privicity"),
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () => Navigator.pushNamedAndRemoveUntil(
                    context, '/home', (route) => false),
                // Here will be code
              ),
            ],
          ),
        ));
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutterapp/rootPages/AboutPage.dart';
import 'package:flutterapp/rootPages/infoPage.dart';
import 'package:flutterapp/subPages/donatePage.dart';


import 'package:url_launcher/url_launcher.dart';

import 'helpers/header.dart';

void main() => runApp(Main());

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter App',
      theme: ThemeData(
        primaryColor: Colors.black,
        backgroundColor: Colors.black,
      ),
      initialRoute: '/home',
      routes: {
        '/home': (context) => MainPage(),
        '/about': (context) => AboutPage(),
        '/info': (context) => InfoPage(),
        '/donate': (context) => donatePage(),
      },
    );
  }
}


class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //topBar
      appBar: headerNav(),
      //sidebar
      drawer: sideBar(context),

      //home
      body: CustomScrollView(
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: SizedBox(
              height: 100,
              child: Center(
                child: Text(
                  "Seit fast 20 Jahren\n"
                      "Erfahrung im IT-Bereich",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    fontFamily: 'Adventure',
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: 175,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/cpit.jpg"),
                    fit: BoxFit.cover),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(1),
                    spreadRadius: 7,
                    blurRadius: 10,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              child: Center(
                heightFactor: 1.7,
                child: Text(
                  "Die IT Branche ist ein boomender Markt. Jeden Tag werden irgendwo in der Welt neue Technologien entwickelt, "
                      "neue Geräte und neue Software. Das bedeutet schier unendliche Möglichkeiten. Es bringt aber auch eine gewisse Unübersichtlichkeit mit sich. "
                      "Anwender und Entscheider in den Unternehmen haben es oft schwer die richtigen Ansätze zu finden."
                      "Wir finden in diesem riesigen Markt Konzepte und Produkte, die Unternehmen wirklich helfen Zeit und Geld zu sparen."
                      "Software und Hardware gehen dabei Hand in Hand und liefern im Ergebnis eine Lösung."
                      "Und das tun wir schon seit dem Jahr 2002 mit Leidenschaft für unsere Kunden. Unser Team lebt das Motto „Fast-Forward IT Support“."
                      "Das heißt wir sind in aller erste Linie Dienstleister und verstehen uns als Partner unserer Kunden.",
                  style: TextStyle(
                      wordSpacing: 6,
                      fontSize: 15),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              child: Center(
                heightFactor: 1.3,
                child:
                FlatButton(
                  color: Colors.transparent,
                  shape: StadiumBorder(),
                  onPressed: () {
                    _launchURL();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25.0),
                      boxShadow: [BoxShadow(
                        color: Colors.grey.withOpacity(1),
                        spreadRadius: 7,
                        blurRadius: 10,
                        offset: Offset(0, 3),
                      ),],
                      image: DecorationImage(
                        image: AssetImage("assets/images/btn.gif"),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                    child:
                    Text("Mehr", style: TextStyle(fontSize: 12)),
                    height: 40,
                    alignment: Alignment.center,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future <void> _launchURL() async {
    const url = 'https://com-plus-it.de';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
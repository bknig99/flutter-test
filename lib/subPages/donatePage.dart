import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../main.dart';

// ignore: camel_case_types
class donatePage extends StatefulWidget {
  // Here will be code
  @override
  _NewPageState createState() => _NewPageState();
}

class _NewPageState extends State<donatePage> {
  // Here will be code

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Donation Page",
            style: TextStyle(
              fontSize: 24,
            )),
      ),
      body: Center(
        child: Column(children: <Widget>[
          SizedBox(height: 50),
          Text(
              "Hier können Sie uns Geld spenden. Sie werden jedoch nicht gezwungen.\n"
              "Aber bitte Spenden Sie, danke."),
          SizedBox(height: 50),
          RaisedButton(
              child: Text('Donate per 1-click ;)'),
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              onPressed: () => Navigator.pushAndRemoveUntil(
                  context,
                  PageTransition(
                      type: PageTransitionType.fade,
                      duration: Duration(milliseconds: 10),
                      child: MainPage()),
                  (r) => true)),
        ]),
      ),
    );
  }
}

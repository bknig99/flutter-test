import 'package:flutter/material.dart';

import 'content.dart';

AppBar headerNav() {
  return AppBar(
    toolbarHeight: 65,
    actions: <Widget>[
      Drawer(
        child: InkWell(
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/Bild2.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
    ],
  );
}

//Sidebar for golbal use
Drawer sideBar(BuildContext context) {
  return Drawer(
    child: Container(
      color: Colors.black87,
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.black,
              image: DecorationImage(
                image: AssetImage("assets/images/source.gif"),
                fit: BoxFit.cover,
              ),
            ),
            child: Text(
              'Test App with Flutter',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          const SizedBox(height: 15),
          FlatButton(
            onPressed: () {
              ContentPage().contentDisplay(context, value: "Home");
            },
            child: const Text('Home',
                style: TextStyle(color: Colors.white, fontSize: 14)),
          ),
          const SizedBox(height: 15),
          FlatButton(
            onPressed: () {
              ContentPage().contentDisplay(context, value: "Info");
            },
            child: const Text('Info',
                style: TextStyle(color: Colors.white, fontSize: 14)),
          ),
          const SizedBox(height: 15),
          FlatButton(
            onPressed: () {
              ContentPage().contentDisplay(context, value: "About");
            },
            child: const Text('About us',
                style: TextStyle(color: Colors.white, fontSize: 14)),
          ),
          const SizedBox(height: 225),
          RaisedButton(
            color: Colors.transparent,
            shape: StadiumBorder(),
            onPressed: () {
              ContentPage().contentDisplay(context, value: "donate");
            },
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25.0),
                image: DecorationImage(
                  image: AssetImage("assets/images/giphy.gif"),
                  fit: BoxFit.cover,
                ),
              ),
              child: Text("Donate please", style: TextStyle(fontSize: 16)),
              padding: EdgeInsets.symmetric(horizontal: 70.0, vertical: 15.0),
              height: 50,
              alignment: Alignment.center,
            ),
          ),
        ],
      ),
    ),
  );
}

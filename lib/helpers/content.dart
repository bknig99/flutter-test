import 'package:flutter/material.dart';
import 'package:flutterapp/rootPages/AboutPage.dart';
import 'package:flutterapp/rootPages/infoPage.dart';
import 'package:flutterapp/subPages/donatePage.dart';
import '../main.dart';

import 'package:page_transition/page_transition.dart';

class ContentPage extends StatelessWidget {
  ContentPage({this.title});

  final title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            contentDisplay(context),
          ],
        ),
      ),
    );
  }

  contentDisplay(BuildContext context, {@required value}) async {
    if (value == "donate") {
      print("open donation");
      Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeftWithFade,
          duration: Duration(milliseconds: 150),
          child: donatePage()));
    } else if (value == "About") {
      print("open about");
      Navigator.pushAndRemoveUntil(context, PageTransition(type: PageTransitionType.fade,
          duration: Duration(milliseconds: 10),
          child: AboutPage()),(r) => false);
    } else if (value == "Info") {
      print("open Info");
      Navigator.pushAndRemoveUntil(context, PageTransition(type: PageTransitionType.fade,
          duration: Duration(milliseconds: 10),
          child: InfoPage()), (r) => true);
    } else if (value == "Home") {
      print("open Home");
      Navigator.pushAndRemoveUntil(context, PageTransition(type: PageTransitionType.fade,
          duration: Duration(milliseconds: 10),
          child: MainPage()),(r) => true);
    }
  }
}
